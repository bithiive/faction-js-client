# Faction JS Client
A simple JS client with no dependencies for interacting with Faction.

## Sample usage:
```
FactionClient = require('faction-client')
client = new FactionClient

// use this key for future requests with this client
client.auth({public: ..., private: ...})

client.account.get(function(err, account) {
    // do something with the account
})
```

## Methods
### Account handling (client.account.*)
* `create(contact, callback)` - create a new account
* `activate(key, callback)` - activate an account
* `get(callback)` - retrieve the account of this key
* `update(contact, callback)` - update the contact information
* `update_key(name, options, callback)` - update/generate an API key
* `remove_key(name, callback)` - remove an API key

### Action handling (client.action.\*, client.actions.\*)
* `types(callback)` - list available action types
* `list([fact type], [paging options], callback)` - list actions against this fact type
* `get(fact type, action id, callback)` - get a specific action
* `update(fact type, action id, action, callback)` - update an action
* `remove(fact type, action id, callback)` - remove an action
* `test(fact type, action id, fact id, [force], callback)` - test an action against a fact
* `exec(fact type, action id, fact id, [stage], callback)` - queue an action against a fact
* `history(fact type, action id, fact id, callback)` - show action history using a fact

### Condition handling (client.condition.\*, client.conditions.\*)
* `list(fact type, [paging], callback)` - list all conditions against this fact type
* `get(fact type, condition id, callback)` - get a condition
* `update(fact type, condition id, condition, callback)` - update a condition
* `remove(fact type, condition id, callback)` - remove a condition
* `test(fact type, condition id, fact id, callback)` - test a condition against a fact

### Fact handling (client.fact.\*, client.facts.\*)
* `types(callback)` - list all known fact types
* `list(type, [paging], callback)` - list facts of a specific type
* `get(type, id, callback)` - get a specific fact
* `update(type, id, fact, callback)` - update a fact
* `remove(type, id, callback)` - remove a fact
* `remove_all(type, callback)` - remove all facts of this type
* `get_settings(type, callback)` - retrieve settings for this fact
* `update_settings(type, settings, callback)` - update settings for this fact

### Info handling (client.info.*)
* `create(type, info, callback)` - submit new information
* `get_mappings(type, [paging], callback)` - list mappings against this type
* `update_mapping(type, id, mapping, callback)` - update a mapping against this type
* `remove_mapping(type, id, callback)` - remove a mapping against this type
