class FactionHook

	constructor: (@api) -> null

	list: (callback) ->
		path = ['hooks']
		@api._send 'GET', path, callback

	get: (id, callback) ->
		@api._send 'GET', ['hooks', id], callback

	update: (id, data, callback) ->
		@api._send 'POST', ['hooks', id], {body: data}, callback

	remove: (id, callback) ->
		@api._send 'DELETE', ['hooks', id], callback

# Module Definitions
if module?
	module.exports = FactionHook
# AMD/RequireJS
if define?
	define 'factionClient-hook', [], -> FactionHook

if angular?
	angular.module('faction.client.hook', []).factory 'FactionHook', -> FactionHook

if jQuery?
	window.FactionHook = FactionHook
