class FactionAccount

	constructor: (@api) -> null

	create: (data, callback) ->
		@api._send 'POST', 'account', {body: data}, callback

	activate: (key, callback) ->
		@api._send 'POST', ['account', 'activate', key], callback

	get: (callback) ->
		@api._send 'GET', 'account', callback

	update: (data, callback) ->
		@api._send 'POST', 'account/contact', {body: data}, callback

	update_key: (name, data, callback) ->
		if typeof data is 'function'
			callback = data
			data = {}

		@api._send 'POST', ['account/key', name], {body: data}, callback

	remove_key: (name, callback) ->
		@api._send 'DELETE', ['account/key', name], callback

	services: (callback) ->
		@api._send 'GET', ['account/services'], callback

	update_service: (name, data, callback) ->
		@api._send 'POST', ['account/services', name], {body: data}, callback

	remove_service: (name, callback) ->
		@api._send 'DELETE', ['account/services', name], callback

	trigger_service_update: (callback) ->
		@api._send 'POST', 'account/update-services', callback


# Module Definitions
if module?
	module.exports = FactionAccount
# AMD/RequireJS
if define?
	define 'factionClient-account', [], -> FactionAccount

if angular?
	angular.module('faction.client.account', []).factory 'FactionAccount', -> FactionAccount

if jQuery?
	window.FactionAccount = FactionAccount
