class FactionTag

	constructor: (@api) -> null

	list: (fact_type, options, callback) ->
		if typeof options is 'function'
			callback = options
			options = {}
		@api._send 'GET', ['tags', fact_type], options, callback

	get: (fact_type, tag_id, callback) ->
		@api._send 'GET', ['tags', fact_type, tag_id], callback

	update: (fact_type, tag_id, data, callback) ->
		@api._send 'POST', ['tags', fact_type, tag_id], {body: data}, callback

	remove: (fact_type, tag_id, callback) ->
		@api._send 'DELETE', ['tags', fact_type, tag_id], callback

	test: (fact_type, tag_id, data, callback) ->
		@api._send 'POST', ['tags', fact_type, tag_id, 'test'], {body: data}, callback

	test_fact: (fact_type, tag_id, fact_id, callback) ->
		@api._send 'POST', ['tags', fact_type, tag_id, 'test', fact_id], callback

# Module Definitions
if module?
	module.exports = FactionTag

# AMD/RequireJS
if define?
	define 'factionClient-tag', [], -> FactionTag

if angular?
	angular.module('faction.client.tag', []).factory 'FactionTag', -> FactionTag

if jQuery?
	window.FactionTag = FactionTag
