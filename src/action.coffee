class FactionAction

	constructor: (@api) -> null

	types: (callback) ->
		@api._send 'GET', '/action-types', callback

	list: (fact_type, options, callback) ->
		if typeof options is 'function'
			callback = options
			options = {}

		path = ['actions']
		path.push fact_type if fact_type

		@api._send 'GET', ['actions', fact_type], options, callback

	get: (fact_type, action_id, callback) ->
		@api._send 'GET', ['actions', fact_type, action_id], callback

	update: (fact_type, action_id, data, callback) ->
		@api._send 'POST', ['actions', fact_type, action_id], {body: data}, callback

	remove: (fact_type, action_id, callback) ->
		@api._send 'DELETE', ['actions', fact_type, action_id], callback

	test: (fact_type, action_id, fact_id, force, callback) ->
		if typeof force is 'function'
			callback = force
			force = false
		@api._send 'GET', ['actions', fact_type, action_id, 'test', fact_id], {body: {force: force}}, callback

	exec: (fact_type, action_id, fact_id, stage, callback) ->
		if typeof stage is 'function'
			callback = stage
			stage = 0
		@api._send 'GET', ['actions', fact_type, action_id, 'exec', fact_id, stage], callback

	history: (fact_type, action_id, fact_id, callback) ->
		@api._send 'GET', ['actions', fact_type, action_id, 'history', fact_id], callback

# Module Definitions
if module?
	module.exports = FactionAction

# AMD/RequireJS
if define?
	define 'factionClient-action', [], -> FactionAction

if angular?
	angular.module('faction.client.action', []).factory 'FactionAction', -> FactionAction

if jQuery?
	window.FactionAction = FactionAction
