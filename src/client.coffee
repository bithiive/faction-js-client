class FactionClient

	constructor: (@host = 'api.faction.io', @port = 9876, http = false) ->
		@key = {}
		@angularHTTP = FactionClient.angularHTTP or http

		@account = new FactionClient.Account @
		@info = new FactionClient.Info @
		@fact = @facts = new FactionClient.Fact @
		@hook = @hooks = new FactionClient.Hook @
		@action = @actions = new FactionClient.Action @
		@misc = new FactionClient.Misc @
		@notification = @notifications = new FactionClient.Notification @

	ping: (callback) ->
		@_send 'GET', '/ping', {auth: false}, callback

	setKey: (@key) -> @

	make_hash: (path, body, key = @key) ->
		hash_parts = [
			path,
			JSON.stringify(body or {})
		]
		return @_encrypt hash_parts, key.private

	_encrypt: (parts, key) ->
		hash = require('crypto').createHmac('sha256', key)
		for part in parts
			hash.update part, 'utf8'
		return hash.digest('hex')

	_send: (method, path, options, callback) ->
		args = Array::slice.call(arguments, 2)
		callback = args.pop()
		options = args.pop() or {}

		body = options.body or {}
		queryObj = options.query or {}

		if path.join
			path = path.join '/'

		path = '/' + path.replace(/^\//, '')

		if options.auth isnt false
			if not @key
				return callback 'This route is HMAC-authorised, but no keypair has been provided'

			queryObj.key = @key.public
			if @key.private and @key.secure isnt false
				queryObj.hash = @make_hash path, body


		body = (body and JSON.stringify(body)) or false

		method = method.toUpperCase()
		if body and not (method in ['POST', 'PUT'])
			queryObj.body = body

		query = (encodeURIComponent(k) + '=' + encodeURIComponent(v) for k, v of queryObj)

		opts =
			host: @host,
			port: @port,
			path: path + '?' + query.join('&')
			method: method

		@_request opts, body, (err, res, data) ->
			try
				if err then throw err

				if typeof data is 'string'
					json = JSON.parse(data)
					if json.code
						throw json.message
				else
					json = data

				opts.statusCode = res.statusCode
				opts.headers = res.headers

				if 0 isnt Math.floor (res.statusCode - 200) / 200
					throw 'Statuscode wasnt 2xx'

				return callback null, json, opts
			catch e
				return callback e, data, opts

	_request: (options, data, callback) ->
		cb = (a, b, c) ->
			callback a, b, c
			cb = () -> null

		if typeof @angularHTTP is 'function'
			opts = {}
			url = "#{options.host}:#{options.port}#{options.path}"

			if data and options.method in ['POST', 'PUT']
				opts = data

			req = @angularHTTP[options.method.toLowerCase()](url, opts)
				.success (data, status, headers, config) ->
					cb null, { statusCode: status }, data

				.error (data, status, headers, config) ->
					err = data or 'Unknown error'
					cb err, { statusCode: status }, data
		else
			http = require 'http'

			req = http.request options, (res) ->
				data = ''
				res.on 'data', (chunk) -> data += chunk

				res.on 'end', () -> cb null, res, data
				res.on 'error', (err) -> return cb error

			if data and options.method in ['POST', 'PUT']
				req.write data

			req.end()
			req.on 'error', cb

			return req

FactionModules = ['Account', 'Hook', 'Info', 'Fact', 'Action', 'Misc', 'Notification']

# Node & CommonJS
if require? and ( module? or exports? )
	for mod in FactionModules
		global['Faction' + mod] or= require './' + mod.toLowerCase()
		FactionClient[mod] = global['Faction' + mod] # eval?

	if module?
		module.exports = FactionClient
	else
		module.id = 'faction-client'
		exports = FactionClient

# AMD
if define?
	list = FactionModules.map (name) -> 'factionClient-' + name.toLowerCase()
	list.push 'CryptoJS-HmacSHA256'

	define 'factionClient', list, () ->
		args = Array::slice.call arguments
		for mod in FactionModules
			FactionClient[mod] = args.shift()
		FactionClient::_encrypt = args.pop()

# Angular
if angular?
	list = FactionModules.map (name) -> 'faction.client.' + name.toLowerCase()
	list.push 'faction.client.crypto'
	client = angular.module 'FactionClient', list
	client.factory 'FactionClient', ($http, FactionAccount, FactionHook, FactionInfo, FactionFact, FactionAction, FactionMisc, FactionNotification, HmacSHA256) ->
		args = Array::slice.call arguments, 1
		for mod in FactionModules
			FactionClient[mod] = args.shift()
		FactionClient::_encrypt = args.pop()
		FactionClient.angularHTTP = $http
		return FactionClient

# jQuery
if jQuery?
	for key in FactionModules
		FactionClient[key] = window['Faction' + key]

	FactionClient::_encrypt = CryptoJSHMAC

	FactionClient::_request = (options, data, callback) ->
		url = 'http://' + options.host
		if options.port isnt 80
			url = url + ':' + options.port
		url += options.path

		if data and options.method in ['GET', 'DELETE']
			data = false

		jQuery.ajax url, {
			# cache: false,
			contentType: 'application/json',
			data: data,
			dataType: 'json',
			type: options.method,
			success: (data, message, jqXHR) ->
				callback null, { statusCode: jqXHR.status }, data
			error: (jqXHR, error, errorThrown) ->
				err = jqXHR.responseJSON ? jqXHR.responseText ? error
				callback err, { statusCode: jqXHR.status }, error
		}

	window.FactionClient = FactionClient

# Appcelerator Titanium
# TODO: Check if a better alternative to CryptoJS for Titanium
if Ti?.Network?
	FactionClient::_request = (options, data, callback) ->
		url = 'http://' + options.host
		if options.port isnt 80
			url = url + ':' + options.port
		url += options.path

		if data and options.method in ['GET', 'DELETE']
			data = false

		xhrpost = Ti.Network.createHTTPClient
			'onload': ( e ) ->
				try
					data = JSON.parse @responseText
				catch
					data = @responseText

				callback null, { 'statusCode': @status }, data

			'onerror': ( e ) ->
				try
					data = JSON.parse @responseText
				catch
					data = @responseText

				callback data, { 'statusCode': @status }, data

		xhrpost.open options.method, url

		if data
			xhrpost.setRequestHeader 'Content-Type', 'application/json'
			xhrpost.setRequestHeader 'charset', 'utf-8'
			xhrpost.send JSON.stringify data
		else
			xhrpost.send( )

