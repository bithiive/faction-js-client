class FactionInfo
	constructor: (@api) -> null

	create: (type, data, callback) ->
		@api._send 'POST', ['info', type], {body: data}, callback

	get_mappings: (type, options, callback) ->
		if typeof options is 'function'
			callback = options
			options = {}
		@api._send 'GET', ['info', type, 'mappings'], options, callback

	get_mapping: (type, id, callback) ->
		@api._send 'GET', ['info', type, 'mappings', id], callback

	update_mapping: (type, id, data, callback) ->
		@api._send 'POST', ['info', type, 'mappings', id], {body: data}, callback

	remove_mapping: (type, id, callback) ->
		@api._send 'DELETE', ['info', type, 'mappings', id], callback

	get_schema: (type, callback) ->
		@api._send 'GET', ['info', type, 'schema'], callback

	update_schema: (type, data, callback) ->
		@api._send 'POST', ['info', type, 'schema'], {body: data}, callback
	
	test_schema: (type, data, callback) ->
		@api._send 'POST', ['info', type, 'schema', 'test'], {body: data}, callback

	remove_schema: (type, callback) ->
		@api._send 'DELETE', ['info', type, 'schema'], callback


# Module Definitions
if module?
	module.exports = FactionInfo
# AMD/RequireJS
if define?
	define 'factionClient-info', [], -> FactionInfo

if angular?
	angular.module('faction.client.info', []).factory 'FactionInfo', -> FactionInfo

if jQuery?
	window.FactionInfo = FactionInfo
