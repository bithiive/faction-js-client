class FactionMisc
	constructor: (@api) -> null

	export: (data, callback) ->
		@api._send 'GET', 'export', {body: data}, callback

	import: (data, callback) ->
		@api._send 'POST', 'import', {body: data}, callback

# Module Definitions
if module?
	module.exports = FactionMisc
	
# AMD/RequireJS
if define?
	define 'factionClient-misc', [], -> FactionMisc

if angular?
	angular.module('faction.client.misc', []).factory 'FactionMisc', -> FactionMisc

if jQuery?
	window.FactionMisc = FactionMisc
