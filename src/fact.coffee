class FactionFact
	constructor: (@api) -> null

	types: (callback) ->
		@api._send 'GET', '/facts', callback

	list: (type, data, callback) ->
		args = Array::slice.call(arguments, 1)
		callback = args.pop()
		data = args.pop() or {}
		options = {body: data}

		if options.body.paging?
			options.query = options.body.paging
			delete options.body.paging

		@api._send 'GET', ['facts', type], options, callback

	get: (type, id, data, callback) ->
		args = Array::slice.call(arguments, 2)
		callback = args.pop()
		data = args.pop() or {}

		@api._send 'GET', ['facts', type, 'fact', id], {body: data}, callback

	update: (type, id, data, callback) ->
		@api._send 'POST', ['facts', type, 'fact', id], {body: data}, callback

	remove: (type, id, callback) ->
		@api._send 'DELETE', ['facts', type, 'fact', id], callback

	remove_all: (type, callback) ->
		@api._send 'DELETE', ['facts', type], callback

	trigger_update: (type, id, callback) ->
		@api._send 'POST', ['facts', type, 'update', id], callback

	trigger_update_all: (type, data, callback) ->
		args = Array::slice.call arguments, 1
		callback = args.pop()
		data = args.pop() or {}

		@api._send 'POST', ['facts', type, 'update'], {body: data}, callback

	get_settings: (type, callback) ->
		@api._send 'GET', ['facts', type, 'settings'], callback

	update_settings: (type, data, callback) ->
		@api._send 'POST', ['facts', type, 'settings'], {body: data}, callback

	remove_settings: (type, callback) ->
		@api._send 'DELETE', ['facts', type, 'settings'], callback

	get_hooks: (type, callback) ->
		@api._send 'GET', ['facts', type, 'hooks'], callback

	update_hook: (type, id, data, callback) ->
		@api._send 'POST', ['facts', type, 'hooks', id], {body: data}, callback

	remove_hook: (type, id, callback) ->
		@api._send 'DELETE', ['facts', type, 'hooks', id], callback

	get_metrics: (type, callback) ->
		@api._send 'GET', ['facts', type, 'metrics'], callback

	get_metric: (type, id, data, callback) ->
		@api._send 'GET', ['facts', type, 'metrics', id], {body: data}, callback

	update_metric: (type, id, data, callback) ->
		@api._send 'POST', ['facts', type, 'metrics', id], {body: data}, callback
	
	remove_metric: (type, id, callback) ->
		@api._send 'DELETE', ['facts', type, 'metrics', id], callback

	backfill_metric: (type, id, count, callback) ->
		@api._send 'POST', ['facts', type, 'metrics', id, 'backfill', count], callback

	test_metric: (type, id, callback) ->
		@api._send 'GET', ['facts', type, 'metrics', id, 'test'], callback

# Module Definitions
if module?
	module.exports = FactionFact
# AMD/RequireJS
if define?
	define 'factionClient-fact', [], -> FactionFact

if angular?
	angular.module('faction.client.fact', []).factory 'FactionFact', -> FactionFact

if jQuery?
	window.FactionFact = FactionFact
