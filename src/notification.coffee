class FactionNotification

	constructor: (@api) -> null

	list: (data, callback) ->
		if typeof data is 'function'
			callback = data
			data = {}

		@api._send 'GET', ['notifications'], {body: data}, callback

	get: (notification_id, callback) ->
		@api._send 'GET', ['notifications', notification_id], callback

	seen: (notification_id, callback) ->
		@api._send 'DELETE', ['notifications', notification_id], callback

	create: (type, group, priority, data, callback) ->
		body = {
			type: type,
			group: group,
			priority: priority | 0,
			info: data
		}
		@api._send 'POST', ['notifications'], {body: body}, callback

# Module Definitions
if module?
	module.exports = FactionNotification
	
# AMD/RequireJS
if define?
	define 'factionClient-notification', [], -> FactionNotification

if angular?
	angular.module('faction.client.notification', []).factory 'FactionNotification', -> FactionNotification

if jQuery?
	window.FactionNotification = FactionNotification
