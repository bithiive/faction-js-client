// Generated by CoffeeScript 1.8.0
(function() {
  var FactionHook;

  FactionHook = (function() {
    function FactionHook(api) {
      this.api = api;
      null;
    }

    FactionHook.prototype.list = function(callback) {
      var path;
      path = ['hooks'];
      return this.api._send('GET', path, callback);
    };

    FactionHook.prototype.get = function(id, callback) {
      return this.api._send('GET', ['hooks', id], callback);
    };

    FactionHook.prototype.update = function(id, data, callback) {
      return this.api._send('POST', ['hooks', id], {
        body: data
      }, callback);
    };

    FactionHook.prototype.remove = function(id, callback) {
      return this.api._send('DELETE', ['hooks', id], callback);
    };

    return FactionHook;

  })();

  if (typeof module !== "undefined" && module !== null) {
    module.exports = FactionHook;
  }

  if (typeof define !== "undefined" && define !== null) {
    define('factionClient-hook', [], function() {
      return FactionHook;
    });
  }

  if (typeof angular !== "undefined" && angular !== null) {
    angular.module('faction.client.hook', []).factory('FactionHook', function() {
      return FactionHook;
    });
  }

  if (typeof jQuery !== "undefined" && jQuery !== null) {
    window.FactionHook = FactionHook;
  }

}).call(this);
